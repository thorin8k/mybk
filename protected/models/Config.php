<?php

/**
 * This is the model class for table "config".
 *
 * The followings are the available columns in table 'config':
 * @property integer $id
 * @property integer key_cfg
 * @property integer $value
 */
class Config extends CActiveRecord
{
        const DBPStatus = 'dropState';
        const DBPToken = 'dropToken';
        const DBPSecret = 'dropTokenSec';
        const BKPath = 'bkpath';
        const DBPPath = 'dbppath';
        const DBHost = 'dbhost';
        const DBUser = 'dbuser';
        const DBPwd = 'dbpwd';
        const AdmMail = 'usrmail';
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Config the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'config';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('key_cfg, value', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, key_cfg, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'key' => 'Key',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('key',$this->key);
		$criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public static function addOrSetConfig($key,$value){
            $cfg;
            $cfgQ = Config::model()->find("key_cfg = '$key'");
            if(!empty($cfgQ)){
                $cfg = Config::model()->findByPk($cfgQ->id);
            }else{
                $cfg = new Config();
                $cfg->key_cfg = $key;
            }
            $cfg->value = $value;
            $cfg->save();
        }
        public static function getConfig($key){
            $cfgQ = Config::model()->find("key_cfg = '$key'");
            return $cfgQ != null ? $cfgQ->value : null;
        }
}